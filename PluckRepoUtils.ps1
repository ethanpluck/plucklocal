﻿Import-Module WebAdministration
Import-Module .\ScheduledTaskFolders.ps1
Import-Module .\TaskScheduler\TaskScheduler.psd1

function stopIIS
{
    iisreset /stop
}

function startIIS
{
    iisreset /start
}

function setGitHome
{
    $env:HOME = 'C:\PluckLocalTools\Git\'
}

function getTestRepo
{
    if ( (Test-Path -Path 'C:\pluck2\sitelifetestdata') -eq $false )
    {
        New-Item -Path 'C:\pluck2\sitelifetestdata' -ItemType 'Container'
    }

    svn 'co' 'http://repo.l3.pluck.com/sltestdata' 'C:\pluck2\sitelifetestdata' '--username=build' '--password=build'
}

function destroyTestRepo
{
    if ( Test-Path -Path 'C:\pluck2\sitelifetestdata' )
    {
    	Set-Location -Path 'C:\'
        Remove-Item -Path 'C:\pluck2\sitelifetestdata' -Recurse -Force
    }
}

function getSLDEPLOY
{
    if ( (Test-Path -Path 'C:\pluck2\sldeploy') -eq $false )
    {
        New-Item -Path 'C:\pluck2\sldeploy' -ItemType 'Container'
    }

    svn 'co' 'http://repo.l3.pluck.com/sldeploy' 'C:\pluck2\sldeploy' '--username=build' '--password=build'
}

function updateSLDEPLOY
{
    if ( Test-Path -Path 'C:\pluck2\sldeploy' )
    {
        Set-Location -Path 'C:\pluck2\sldeploy'
        svn 'update'
    }
}

function destroySLDEPLOY
{
    if ( Test-Path -Path 'C:\pluck2\sldeploy' )
    {
        Set-Location -Path 'C:\'
        Remove-Item -Path 'C:\pluck2\sldeploy' -Recurse -Force
    }
}

function cleanSLDEPLOY
{
    if ( Test-Path -Path 'C:\pluck2\sldeploy' )
    {
        Set-Location -Path 'C:\pluck2\sldeploy'
        svn 'cleanup'
        svn 'revert' '--depth=infinity' '.'
    }
}

function removeRavenDBFolder
{
    if ( Test-Path -Path 'C:\Pluck\sitelife\external\RavenDBWebServer\Database' -PathType Container )
    {
    	Set-Location -Path 'C:\'
        blockOnDelete 'C:\Pluck\sitelife\external\RavenDBWebServer\Database'
    }
}

function clonePluckRepo ( [switch] $Legacy )
{
    setGitHome

    if ( $Legacy )
    {
        git 'clone' 'git@repo.l3.pluck.com:sitelife' 'C:\pluck2\sitelife'
    }
    else
    {
        git 'clone' 'git@repo.l3.pluck.com:sitelife' 'C:\pluck\sitelife'
    }
}

function cleanPluckRepo ( [switch] $Legacy )
{
    if ( $Legacy )
    {
        $Path = 'C:\pluck2\sitelife'
    }
    else
    {
        $Path = 'C:\pluck\sitelife'
    }

    if ( Test-Path -Path $Path -PathType Container )
    {
        git 'clean' '-f' '-d' '-x'
    }
}

function resetPluckRepo ( [string] $branch, [switch] $Legacy )
{
    if ( $Legacy )
    {
        $Path = 'C:\pluck2\sitelife'
    }
    else
    {
        $Path = 'C:\pluck\sitelife'
    }

    if ( Test-Path -Path $Path -PathType Container )
    {
        git 'reset' '--hard' "$branch"
    }
}

function updatePluckRepo( [switch] $Legacy )
{
    setGitHome

    if ( $Legacy )
    {
        $Path = 'C:\pluck2\sitelife'
    }
    else
    {
        $Path = 'C:\pluck\sitelife'
    }

    if ( Test-Path -Path $Path -PathType Container )
    {
        git pull
    }
}

function destroyPluckRepo ( [switch] $Legacy )
{
    if ( $Legacy )
    {
        $Path = 'C:\pluck2\sitelife'
    }
    else
    {
        $Path = 'C:\pluck\sitelife'
    }

    if ( Test-Path -Path $Path )
    {
    	Set-Location -Path 'C:\'
        blockOnDelete $Path
    }

    return ( Test-Path -Path $Path )
}

function checkoutPluckBranch ( [string] $branch, [switch] $Legacy )
{
    if ( $Legacy )
    {
        $Path = 'C:\pluck2\sitelife'
    }
    else
    {
        $Path = 'C:\pluck\sitelife'
    }

    if ( Test-Path -Path $Path -PathType Container )
    {
        Set-Location -Path 'C:\pluck2\sitelife'

        git 'checkout' "$branch"
    }
}

function buildPluck ( [switch] $Legacy, [switch] $Portfolio )
{
    if ( $Legacy )
    {
        Set-Location -Path 'C:\pluck2\sitelife'
        if ( $Portfolio )
        {
            .\otter_multi.bat
        }
        else
        {
            .\otter.bat
        }
    }
    else
    {
        Set-Location -Path 'C:\pluck\sitelife'
        if( $Portfolio )
        {
            .\otter.cmd '-portfolio'
        }
        else
        {
            .\otter.cmd
        }
    }
}

function destroyPluckTasks
{
    $taskFolder = Get-TaskFolder ( New-TaskObject -Path '\' )
    $pluckTaskFolder = $null
    foreach ( $folder in $taskFolder )
    {
        if ( $folder -eq "\Pluck" )
        {
            $pluckTaskFolder = $folder
        }
    }
    if ( $pluckTaskFolder -ne $null )
    {
        $tasksToDelete = Get-ScheduledTask -Folder 'Pluck'
        if ( $tasksToDelete.Length -gt 0 )
        {
            foreach ( $taskToDelete in $tasksToDelete )
            {
                $taskToDelete | Stop-Task
                $taskToDelete | Remove-Task
            }
        }

        Remove-TaskFolder -Folder (New-TaskObject) -path $pluckTaskFolder
    }
}

function stopAppPool ( [string] $appPoolName )
{
    if ( Test-Path -Path "IIS:\AppPools\$appPoolName" )
    {
        Stop-WebAppPool -Name $appPoolName
    }
}

function startAppPool ( [string] $appPoolName )
{
    if ( Test-Path -Path "IIS:\AppPools\$appPoolName" )
    {
        Start-WebAppPool -Name $appPoolName
    }
}

function stopWebsite ( [string] $websiteName )
{
    if ( Test-Path -Path "IIS:\Sites\$websiteName" )
    {
        Stop-Website -Name $websiteName
    }
}

function startWebsite ( [string] $websiteName )
{
    if ( Test-Path -Path "IIS:\Sites\$websiteName" )
    {
        Start-Website -Name $websiteName
    }
}

function removeAppPool ( [string] $appPoolName )
{
    if ( Test-Path -Path "IIS:\AppPools\$appPoolName" )
    {
        Remove-WebAppPool -Name $appPoolName
    }
}

function removeWebsite ( [string] $websiteName )
{
    if ( Test-Path -Path "IIS:\Sites\$websiteName" )
    {
        Remove-Website -Name $websiteName
    }
}

function shutdownPluck ( [switch] $Destroy )
{
    stopWebsite 'Pluck'
    stopAppPool 'Pluck'
    if ( $Destroy )
    {
        removeWebsite 'Pluck'
        removeAppPool 'Pluck'
    }
    
    stopWebsite 'PluckDashboard'
    stopAppPool 'PluckDashboard'
    if ( $Destroy )
    {
        removeWebsite 'PluckDashboard'
        removeAppPool 'PluckDashboard'
    }

    stopWebsite 'PrefsServer'
    stopAppPool 'PrefsServer'
    if ( $Destroy )
    {
        removeWebsite 'PrefsServer'
        removeAppPool 'PrefsServer'
    }

    stopWebsite 'PluckBuildServer'
    stopAppPool 'PluckBuildServer'
    if ( $Destroy )
    {
        removeWebsite 'PluckBuildServer'
        removeAppPool 'PluckBuildServer'
    }

    stopWebsite 'RavenDB'
    stopAppPool 'RavenDB'
    if ( $Destroy )
    {
        removeWebsite 'RavenDB'
        removeAppPool 'RavenDB'
    }
}

function destroySitelifeDB
{
    if ( Test-Path 'C:\sitelifedb' -PathType Container )
    {
    	Set-Location -Path 'C:\'
        blockOnDelete 'C:\sitelifedb'
    }

    if ( Test-Path 'C:\sitelifedb_portfolio' -PathType Container )
    {
    	Set-Location -Path 'C:\'
        blockOnDelete 'C:\sitelifedb_portfolio'
    }
}

function removePluckFromIIS
{
    shutdownPluck -Destroy
}

function destroyPluckRoot ( [switch] $Legacy )
{
    if ( $Legacy )
    {
        if ( Test-Path -Path 'C:\pluck2' -PathType Container )
        {
        	Set-Location -Path 'C:\'
            blockOnDelete 'C:\pluck2'
        }
    }
    else
    {
        if ( Test-Path -Path 'C:\pluck' -PathType Container )
        {
        	Set-Location -Path 'C:\'
            blockOnDelete 'C:\pluck'
        }
    }
}

function blockOnDelete ( [string] $pathToDelete, $timeout )
{
    if ( ($timeout -gt 0) -ne $true )
    {
        $timeout = 5;
    }

    while ( $timeout -gt 0 )
    {
        try
        {
            Remove-Item -Recurse -Force $pathToDelete -ea 1
            $timeout = 0
        }
        catch [Exception]{
            $timeout -= 1

            Write-Host Error Destroying Path: $pathToDelete -ForegroundColor Red
            Write-Host Exception Details: $_.Exception.Message -ForegroundColor Red

            if ( $timeout -gt 0 )
            {
                Write-Host [$timeout] retries left -ForegroundColor Red
                Write-Host Sleeping for 10 seconds... -ForegroundColor Gray
                Start-Sleep -s 2
            }
            else
            {
                Write-Host Failed to Destroy Path `(No More Retries`): $pathToDelete -ForegroundColor Red
            }
        }
    }
}

function updateCredentials ( [switch] $Legacy )
{
	if ( $Legacy )
	{
		Write-Host updateCredentials Not Yet Supported with Old Pluck
	}
	else
	{
		if ( Test-Path -Path 'C:\pluck\sitelife' -PathType Container )
		{
			Set-Location -Path 'C:\pluck\sitelife'
			.\updateOtterCreds.ps1
		}
	}
}

function clearLegacyIISSettings
{
    if ( (Test-Path 'IIS:\AppPools\DefaultAppPool') -and (Test-Path 'IIS:\Sites\Default Web Site') )
    {
        $apps = Get-WebApplication -Site 'Default Web Site'
        foreach ( $app in $apps )
        {
            Remove-WebApplication -Name ([string]$app.path) -Site 'Default Web Site'
        }

        Set-ItemProperty 'IIS:\Sites\Default Web Site' -Name 'physicalPath' -Value "$env:SystemDrive\inetpub\wwwroot"

        setModernAppPool
    }
}

function setClassicAppPool
{
    if ( Test-Path -Path 'IIS:\AppPools\DefaultAppPool' )
    {
        Set-ItemProperty 'IIS:\AppPools\DefaultAppPool' -Name 'managedRuntimeVersion' -Value 'v2.0'
        Set-ItemProperty 'IIS:\AppPools\DefaultAppPool' -Name 'managedPipelineMode' -Value 1
    }

    if ( Test-Path -Path 'IIS:\Sites\Default Web Site')
    {
        $bindings = Get-ItemProperty -Path 'IIS:\Sites\Default Web Site' -Name 'bindings'
        for ( $i = 0; $i -lt $bindings.Collection.Length; $i++ )
        {
            if ( ($bindings.Collection[$i].protocol -eq 'http') -and ($bindings.Collection[$i].bindingInformation -eq '*:801:') )
            {
                Set-ItemProperty -Path 'IIS:\Sites\Default Web Site' -Name "bindings.Collection[$i].bindingInformation" -Value '*:80:'
            }
        }
    }
}

function setModernAppPool
{
    if ( Test-Path -Path 'IIS:\AppPools\DefaultAppPool' )
    {
        Set-ItemProperty 'IIS:\AppPools\DefaultAppPool' -Name 'managedRuntimeVersion' -Value 'v4.0'
        Set-ItemProperty 'IIS:\AppPools\DefaultAppPool' -Name 'managedPipelineMode' -Value 0
    }

    if ( Test-Path -Path 'IIS:\Sites\Default Web Site')
    {
        $bindings = Get-ItemProperty -Path 'IIS:\Sites\Default Web Site' -Name 'bindings'
        for ( $i = 0; $i -lt $bindings.Collection.Length; $i++ )
        {
            if ( ($bindings.Collection[$i].protocol -eq 'http') -and ($bindings.Collection[$i].bindingInformation -eq '*:80:') )
            {
                Set-ItemProperty -Path 'IIS:\Sites\Default Web Site' -Name "bindings.Collection[$i].bindingInformation" -Value '*:801:'
            }
        }
    }
}