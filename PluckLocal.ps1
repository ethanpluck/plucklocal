﻿Import-Module C:\PluckLocalTools\scripts\PluckRepoUtils.ps1

function Uninstall-Pluck ( [switch] $Legacy, [switch] $KeepSitelifeDB, [switch] $CleanSLDEPLOY )
{
    if ( $Legacy )
    {
        stopWebsite 'Default Web Site'
        stopAppPool 'DefaultAppPool'

        cleanPluckRepo -Legacy

        clearLegacyIISSettings
    }
    else
    {
        removePluckFromIIS
        stopIIS
        removeRavenDBFolder
        cleanPluckRepo
        destroyPluckTasks
    }

    if ( $KeepSitelifeDB -ne $true )
    {
        destroySitelifeDB
    }

    if ( $CleanSLDEPLOY -eq $true )
    {
        cleanSLDEPLOY
    }

    startIIS
}

function Destroy-Pluck ( [switch] $Legacy, [switch] $KeepSitelifeDB )
{
    if ( $Legacy )
    {
        Uninstall-Pluck -Legacy -KeepSitelifeDB:$KeepSitelifeDB
        destroyTestRepo
        destroyPluckRoot -Legacy
    }
    else
    {
        Uninstall-Pluck -KeepSitelifeDB:$KeepSitelifeDB
        removePluckFromIIS
        destroyTestRepo
        destroyPluckRoot
        destroyPluckRoot -Legacy
    }
}

function Setup-Pluck ( [switch] $Legacy )
{
    getTestRepo
    getSLDEPLOY
    clonePluckRepo -Legacy:$Legacy

    updateCredentials -Legacy:$Legacy

    if ( $Legacy )
    {
        setClassicAppPool
    }
    else
    {
        setModernAppPool
    }
}

function Deploy-Pluck ( [string] $branch, [switch] $Legacy, [switch] $Portfolio, [switch] $Clean, [switch] $Redeploy )
{
    if ( [string]::IsNullOrEmpty( $branch ) )
    {
        Write-Host You must specify a branch! e.g. Deploy-Pluck `'Pluck-6.0.22`' or Deploy-Pluck `'REL-5.5.18`' -Legacy
        return;
    }
    if ( $Legacy )
    {
        if ( !$Redeploy )
        {
            Uninstall-Pluck -Legacy -KeepSitelifeDB:(!$Clean) -CleanSLDEPLOY:$Clean
        }

        setClassicAppPool

        updatePluckRepo -Legacy

        if ( !$Redeploy )
        {
            resetPluckRepo $branch -Legacy
        }

        checkoutPluckBranch $branch -Legacy

        updateSLDEPLOY

        startAppPool 'DefaultAppPool'
        startWebsite 'Default Web Site'

        buildPluck -Legacy -Portfolio:$Portfolio
    }
    else
    {
        if ( !$Redeploy )
        {
            Uninstall-Pluck -KeepSitelifeDB:(!$Clean) -CleanSLDEPLOY:$Clean
        }

        setModernAppPool

        updatePluckRepo

        if ( !$Redeploy )
        {
            resetPluckRepo $branch
        }

        checkoutPluckBranch $branch

        updateSLDEPLOY

        # 6.0.23 Otterfest Workaround - Checks out the Fixed otter.ps1 file from the next commit after .23
        if ( $branch.ToUpper().CompareTo( 'PLUCK-6.0.23' ) -eq 0 )
        {
            Set-Location -Path 'C:\pluck\sitelife'
            git checkout c5ea8658b61317e959075de4563599dd11806f27 -- otter.ps1
        }
        elseif ( $branch.ToUpper().CompareTo( 'PLUCK-6.0.27' ) -eq 0 )
        {
            if ( !(Test-Path -Path 'C:\pluck\sitelife\ApiSite' -PathType Container) )
            {
                New-Item -Path 'C:\pluck\sitelife\ApiSite' -ItemType 'directory'
            }

            Set-Location -Path 'C:\pluck\sitelife\ApiSite'
            '{"CodeBranch":null,"CodeRevision":null,"OverridesPath":"","ShortHash":"79c4","LongString":"Branch: , SHA: , Overrides: "}' | Out-File '.\PluckVersionInfo.json' -Encoding utf8 -Force
        }

        buildPluck -Portfolio:$Portfolio
    }
}