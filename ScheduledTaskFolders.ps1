﻿Function Get-ScheduleService
{
  New-Object -ComObject schedule.service
} #end Get-ScheduleService

Function New-TaskObject($path)
{ #returns a taskfolder object
 $taskObject = Get-ScheduleService
 $taskObject.Connect()
 if(-not $path) { $path = "\" }
 $taskObject.GetFolder($path)
} #end New-TaskObject

Function Get-TaskFolder($folder,[switch]$recurse)
{ #returns a string representing the path to a task folder
  if($recurse)
    {
     $colFolders = $folder.GetFolders(0)
      foreach($i in $colFolders)
        {
          $i.path
          $subFolder = (New-taskObject -path $i.path)
           Get-taskFolder -folder $subFolder -recurse
        }
    } #end if
  ELSE
    {
     $folder.GetFolders(0) |
      foreach-object { $_.path }
     } #end else
} #end Get-TaskFolder

Function New-TaskFolder($folder,$path)
{ 
 $folder.createFolder($path)
} #end New-TaskFolder

Function Remove-TaskFolder($folder,$path)
{
 $folder.DeleteFolder($path,$null)
} #end Remove-TaskFolder


# *** Entry point to the script ***

# Get-TaskFolder -folder (New-taskObject -path "\microsoft")
# Get-TaskFolder -folder (New-taskObject -path "\microsoft") -recurse 
# New-TaskFolder -folder (New-taskObject) -path "\mred"
# Remove-TaskFolder -folder (New-taskObject) -path "\mred"